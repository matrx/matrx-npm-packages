# matrx-npm-packages

## Development

1. Clone the repository
2. In the root of the repository, enter `npm run bootstrap`
3. cd into either `packages/radar` or `packages/grid`
4. `npm run dev`

### Using Lerna

We're using the [Lerna](https://github.com/lerna/lerna) tool so we can have   
multiple npm packages in a single git repository. This is an approach referred
to as "monorepos".

The other big thing about Lerna is that you use it to version and publish all
of the npm packages in this repository. As of this writing, I haven't confirmed
that all the packages are configured properly to be versioned and published
this way but we'll figure that out soon enough.

### @matrx npm Organization and Scope

Another big difference about this project compared to what you might be used
to is that all of the MatrX npm packages publish to the `@matrx` organization and are
considered "scoped". I'm new to this also so we'll have to figure this out
together but the first thing you'll notice is that the package names are
different. Instead of `matrx-radar`, it's now `@matrx/radar`. The advantage
is that now all of these packages are clearly the property of MatrX rather than
its founders.

### GitLab

Not sure if this will stick, but for now, the repository is hosted on GitLab
instead of GitHub. There are two reasons for this:

1. The `matrx` organization on GitHub was already claimed. Rather, we had to
claim the `matrx-transformation`. Yuk!
2. GitLab is a more complete platform. In particular, it has a browser-based IDE
which could be very useful for when Larry needs to make edits but he is traveling.
It would allow us to use an iPad for editing... although
not on an airplane since it requires a fast internet connection. It also has
a number of other capabilities that we may be able to take advantage of going
forward like built-in CI/CD.

That said, it's an experiment. We'll see.
